package com.iillyyaa2033.utils;

import java.io.*;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.URLConnection;

public class NetworkUtils {

    public static boolean DEBUG_ATTACHMENT_SENDING = false;

    public static boolean isPortAvailable(int port) {
        try (DatagramSocket ignored = new DatagramSocket(port)) {
            return true;
        } catch (IOException ignored) {
            return false;
        }
    }


    public static String fasterRead(HttpURLConnection session) throws IOException {
        String contents;

        if (session.getContentLength() > 0) { // We're using faster read method if content-length provided
            int contentLength = session.getContentLength();
            byte[] buffer = new byte[contentLength];
            session.getInputStream().read(buffer, 0, contentLength);
            contents = new String(buffer);
        } else { // Otherwise falling back to slower method
            java.util.Scanner s = new java.util.Scanner(session.getInputStream()).useDelimiter("\\A");
            contents = s.hasNext() ? s.next() : "";
        }

        return contents;
    }

    /**
     * Send file as http multipart/form-data
     *
     * @param httpConn  connection write to
     * @param fieldName name of form filed
     * @param path      location of file to send
     * @throws IOException when something went wrong
     */
    public static void multipartFileUpload(HttpURLConnection httpConn, String fieldName, String path, IUploadListener listener) throws IOException {
        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach...");

        // VARIABLES
        String boundary = "===" + System.currentTimeMillis() + "==="; // creates a unique boundary based on time stamp
        String LINE_FEED = "\r\n";
        OutputStream outputStream;
        PrintWriter writer;

        // INIT
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true);    // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        outputStream = httpConn.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream), true);

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: inited");

        // FORM
        String s = "{}";

        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"payload_json\"").append(LINE_FEED);
        writer.append("Content-Type: text/plain; charset=utf8").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(s).append(LINE_FEED);
        writer.flush();

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: form written");

        // FILE
        File uploadFile = new File(path);
        String fileName = uploadFile.getName();
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"").append(LINE_FEED);
        writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName)).append(LINE_FEED);
        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        long size = uploadFile.length();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        int i = 0;
        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Writing");
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            i++;
            outputStream.write(buffer, 0, bytesRead);
            if (listener != null) listener.progress(buffer.length * i, size);
        }
        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Flushing(1)");
        outputStream.flush();
        inputStream.close();
        writer.append(LINE_FEED);

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Flushing(2)");
        writer.flush();

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: file written");

        // SEND
        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();

        if (DEBUG_ATTACHMENT_SENDING) System.out.println("Sending attach: message sent");
    }

    public interface IUploadListener {

        void started();

        void progress(long bytesSent, long bytesTotal);

        void waitingServer();

        void done();

    }
}
