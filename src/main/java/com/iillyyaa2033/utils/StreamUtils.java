package com.iillyyaa2033.utils;

public class StreamUtils {

    /**
     * Read contents of InputStream into string
     * @param is InputStream to read
     * @return string
     */
    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
